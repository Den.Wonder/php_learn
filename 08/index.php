<?php
/*Управляющие конструкции PHP. Цикл while и do-while*/

error_reporting(-1);

//$i = 1;
//while($i <= 10){
//    echo "<p>{$i}-й</p>";
//    $i++;
//};


$i=1;
echo "<table border=\"1\"> \n";
while($i<=10){
    echo "\t <tr> \n";
    $n=1;
    while($n<=10){
        echo "\t\t <td> {$i}*{$n}= " .$i*$n. "</td>\n";
        $n++;
    }
    echo "\t </tr> \n ";
    $i++;
}
echo "\n </table>";

/*$i = 1900;

echo '<select>'."\n";
    while($i <=2019){
        echo "\t <option value='{$i}'>$i</option> \n";
        $i++;
    }
echo '</select>';*/

//$i=11;
//do{
// echo "{$i} <br>";
//}while($i<=10)

?>