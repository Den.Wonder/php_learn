<?php
error_reporting(-1);
$start = microtime(true);
echo '<h1><b>lesson 22 - data & time php functions</b></h1><br>';
 /*
 string date(формат, )
 int time
 string date_default_timezone_get
 bool default_timezone_set
 array getdate
 int strtotime
 int mktime
mixed microtime
--------------------------
 DateTime date_create
 DateTime date_add
 string date_format
 date_interval_create_from_date_string
 DateInterval date_diff


настройка часового пояса в графе timezone файла php.ini
php.net/manual/ru/function.date.php
  */


 echo date_default_timezone_get();
 echo '<br>';
 date_default_timezone_set('Asia/Krasnoyarsk');
 echo '<br>';
echo date_default_timezone_get();
echo '<br>';
 //echo date('Y-m-d H:i:s',873018000);
$date = getdate();
echo $date['year'];
echo '<br>';
echo '<br>';
echo '<br>';

//echo date('Y-m-d- H:i:s', strtotime("+1 day"));

$date = date_create('2019-07-18');
print_r($date);
echo '<br>';
echo '<br>';
echo '<br>';
//var_dump($date);
echo date_format($date,'Y-m-d H:i:s ');
date_add($date, date_interval_create_from_date_string('2days + 2 hours + 45 minutes + 14 seconds'));
echo '<br>';
echo date_format($date, 'Y-m-d H:i:s');
echo '<br>';
echo '<br>';
echo '<br>';


echo time();
echo '<br>';
echo microtime();

$end = microtime(true);

echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';
echo $end-$start;

?>