<?php
error_reporting(-1);
$str = '[i]Привет![/i] Меня зовут [B]Вася[/b]! И я, блять, охуенно рад вас видеть!';

//функция, заменяющая слова в строке. Пример - цензура.
$str = str_replace('блять', 'черт возьми', $str);
$str = str_replace('охуенно', 'безумно', $str);

$search = [
    '[b]', '[/b]',
    '[i]', '[/i]',
];
$replace = [
    '<b>', '</b>',
    '<i>', '</i>',
];

$str = str_replace($search, $replace, $str);
echo $str;
$str = str_ireplace($search, $replace, $str);
echo '<br>';
echo $str;
echo '<br>';

$str = '<i>Привет!</i> Меня зовут <b>Вася</b>!<br>';
//Вырезает из строки все тэги. Второй параметр - тэги, которые можно не удалять
echo strip_tags($str, '<b>');
echo '<br>';

$str = 'hello!';
echo strlen($str);
$str = 'жопа!';
//strlen - считает количество байт, а не количество символов в строке
echo strlen($str);
echo '<br>';
//префикс mb - мультибайтовый. Для символов других кодировок
echo mb_strlen($str, 'utf-8');
?>