<?php
error_reporting(-1);
/*
array_explode
string_implode
string trim
string rtrim
string ltrim
string md5
string sha1
string nl2br
mixed str_replace
mixed str_ireplace
string strip_tags
int strlen

mixed mb_strlen
int mb_strpos







*/
//$str = 'Иванов Иван Иванович';
//$data = explode(' ',$str, 3);
//(разделитель, строка, кол-во элементов)
//echo $data[1];
//$data = [
//    'Чуднов',
//    'Денис',
//    'Дмитриевич'
//];
//$str = implode(' ', $data);
//(разделитель, массив данных)
//echo $str;
//implode = join;

//$str = "\n<p>Hello</p>\n";
//$str .= "\n<p> world</p>\n";
////echo $str;
//echo trim($str);
//обрезает пробелы и переносы - полезно для модулей авторизации, например.
//ltrim и rtrim - обрезают те же символы только сначала или с конца строки соответственно
//можно обрезать другие символы, передавая их вторым параметром:
//trim($str, ',');


//хеширование (шифрование) строки по алгоритму мд5 (для хранения паролей, например)
$str = 'password';
echo md5($str);
echo '<br>';
echo md5(md5($str));
echo '<br>';
echo sha1($str);
echo '<br>';
echo sha1(sha1($str));
echo '<br>';

$str2 = "Hello \n world\n";
echo $str2;
echo '<br>';
//nl2br - превращает символ новой строки в тег br - полезно, например, для форм комментариев!
echo nl2br($str2);

?>