<?php
error_reporting(-1);
echo '<h1>Альтернативный синтарсис php</h1><br>';

//switch - оператор условия для множества вариантов
$var = 2;

/*if($var==1){
    echo 'Variable = 1';
} elseif ($var==2){
    echo 'var = 2';
} elseif($var==3){
    echo 'var = 3';
} else{
    echo 'var = something else';
}*/

/*switch ($var){
    case 1:
        echo 'var = 1';
        break;
    case 2:
        echo 'var = 2';
        break;
    case 3:
        echo 'var = 3';
        break;
    default:
        echo 'var = something else';
        break;
}*/

$bool = true;
$str1 = 1;
$str2 = 2;
$str3 = 3;
/*if ($bool){
    echo "<table class=\"table\" border='1'>
                <tr>
                    <td>".$str1."</td>
                    <td>".$str2."</td>
                    <td>".$str3."</td>
                </tr>
            </table>";
}*/



?>

<?php if($bool): ?>
    <table class=\"table\" border='1'>
        <tr>
            <td><?php echo $str1 ?></td>
            <td><?php echo $str2 ?></td>
            <td><?php echo $str3 ?></td>
        </tr>
    </table>
<?php endif ?>
