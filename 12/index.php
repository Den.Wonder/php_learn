<?php
error_reporting(-1);

echo '<h1>lesson 12 - cycle foreach</h1><br>';
$arr = ['Ivanov', 'Petrov', 'Sidorov'];
$arr[5] = 'Pupkin';
$arr[8] = 'SamBody';

$names = [
    'Ivan' => 'Ivanov',
    'Petr' => 'Petrov',
    'Sidor' => 'Sidorov',
];

//    foreach ($arr as $item){
//        echo $item.'<br>';
//    }

foreach ($names as $name => $surname){
    echo "Name: $name, Surname: $surname <br>";
}

//break - прерывает выполнение цикла

    for($i=0;$i<=30;$i++){
        if($i>=10 && $i<=20) continue ;
        echo $i."<br>";
    }
//continue - используется для пропуска (не выполнения действий)



?>