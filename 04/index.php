<?php
error_reporting(-1);
//Нет строгой типизации - можно не объявлять тип и заменять его другими значениями
//php - слаботипизированный язык
$var = 'pencil';
$is_auth = false; //example of boolean variable
$fl = 1.2; //в качестве разделителя используется только точка!
$str = 'this is string!';
$str2 = "this is {$var}'s!";
$str3 = "this is \"string with ecraning kovichki\"";


/*
 В одинарнных ковычках переменные не распознаяются.
В двойных - распознаются и обрабатываются!
  */
/*
типы данных в php:
boolean - true | false
integer - число
float - число с плавающей точкой
string - строка
 */
//экранируя символ с помощью обратного слэша мы делаем его не специальным! \
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>4 lesson</title>
</head>
<body>
    <h1>
        <?php
        echo "$var   ";
        var_dump($str);
        var_dump($str2);
        var_dump($str3);
        echo gettype($var);
        /*
         если число записать в ковычках - это будет строка
        Но если к числовой строке прибавить число - получим
        число!
         * */
        ?>
    </h1>
<p>and now...</p>
<h3>
    <?php
    $var = 23;
    echo "$var";
    var_dump($var);
    echo gettype($var);
    //HEREDOC-syntax for variables - аналог двойных ковычек
    $str3 = <<<HERE
some string with any symbols, and "" '' {} too $var
HERE;

    $var = 'var';

echo $str3;

//NOWDOC - аналог одинарных ковычек
    $str4 =<<<'HERE'
this 'is' "string" $var
HERE;

    echo $str4;
    
    ?>
</h3>
</body>
</html>
