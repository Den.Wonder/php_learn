<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css"/>
    <title>Registration page</title>
</head>
<body><?php
require ('connect.php');
if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email'])){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $query = "INSERT INTO users (username, email, password) VALUES ('$username', '$email', '$password')";
    $result = mysqli_query($connection, $query);

    if($result){
        $smsg = "Регистрация прошла успешно!";
    } else {
        $fmsg = "Ошибка";
    }

}
?>

    <div class="container">
        <form class="form-signin" method="POST">
            <h2>Регистрация</h2>
            <br>
            <?php
            if(isset($smsg)){
                ?>
            <div class="alert alert-success" role="alert">
                <?php echo $smsg;  ?>
            </div>
        <?php
            }
            ?>
            <?php
            if(isset($fmsg)){
            ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $fmsg; ?>
        </div>
        <?php
            }
            ?>

            <p>
                <input type="text" name="username" class="form-control" placeholder="Username" required>
            </p>
            <p>
                <input type="text" name="email" class="form-control" placeholder="Email" required>
            </p>
            <p>
                <input type="password" name="password" class="form-control" placeholder="Password" required>
            </p>
            <p>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Регистрация</button>
            </p>
            <p>
                <a href="login.php" class="btn btn-lg btn-primary btn-block" >Авторизация</a>
            </p>
        </form>
    </div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
