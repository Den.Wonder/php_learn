<?php
session_start();
require ('connect.php');
if(isset($_POST['username']) and isset($_POST['password'])){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $query = "SELECT * FROM users WHERE username = '$username' and password ='$password'";
    $result = mysqli_query($connection, $query) or die(mysqli_error($connection));
    $count = mysqli_num_rows($result);
    if($count==1){
        $_SESSION['username'] = $username;
    } else{
        $fmsg = "Ощибка!";
    }

    if (isset($_SESSION['username'])){
        $username = $_SESSION['username'];
        echo "Hello, ". $username . "!" . '<br>' . "Добро пожаловать!";
        echo "<br><a href='logout.php' class='btn btn=lg btn-primary'> logout </a>";

    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css"/>
    <title>Registration page</title>
</head>
<body>
<div class="container">
    <form class="form-signin" method="POST">
        <h2>Авторизация</h2>
        <br>
        <p>
            <input type="text" name="username" class="form-control" placeholder="Username" required>
        </p>
        <p>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
        </p>
        <p>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
        </p>
        <p>
            <a href="index.php" class="btn btn-lg btn-primary btn-block" >Регистрация</a>
        </p>
    </form>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
