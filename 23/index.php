<?php
error_reporting(-1);
/*данные сначала попадают в массивы _POST и_GET*/
echo '<h1><p><b>GET & POST METHODS on PHP</b></p></h1>';
echo '<br>';
echo '<br>';
echo '<br>';
if(isset($_POST['send'])){
    echo '<pre>';
    print_r($_POST);
    echo '</pre>';
}
echo '<br>';
if(isset($_GET['send'])){
    echo '<pre>';
    print_r($_GET);
    echo '</pre>';
}
echo '<br>';
?>
<html>
<head>
    <meta charset="utf-8">
    <title>lesson 23 - GET & POST METHODS on PHP</title>
</head>
<body>
    <form method="post" action="">
        <p>
            <input type="text" name="name">
        </p>
        <P>
            <textarea name="text">

            </textarea>
        </P>
        <p>
            <input type="checkbox" name="remember">
        </p>
        <p>
            <button type="submit" name="sand" value="test">Send</button>
        </p>
    </form>
    <hr>
    <p>
        Введенное имя:
        <?php if(isset($_POST['name'])){
            echo $_POST['name'];
        } else{
            echo 'Форма не отправлена!';
        } ?>
    </p>
    <p>
        Введенный текст:
<!--        -->
        <?php
        //if(isset($_POST['text'])){
//            echo nl2br($_POST['text']);
//        } else{
//            echo 'Форма не отправлена!';
//        }
        //
         ?>

        <?php echo isset($_POST['text']) ? nl2br($_POST['text']) : 'форма не отправлена!'; ?>

        <?php
        if(isset($_POST['remember']) && $_POST['remember']=='on'){
            echo '<pre>';
            echo 'Чекбокс отмечен!!!!';
            echo '</pre>';
        }
        echo '<br>';
        if(isset($_GET['send'])){
            echo '<pre>';
            print_r($_GET);
            echo '</pre>';
        }
        echo '<br>';
        ?>

    </p>
</body>
</html>

