<?php
//функция вывода вообще всех ошибок
error_reporting(-1);

$xuk = 'Xun!';
$title = 'page title';
$fruit = 'apple';
//переменные в одинарных ковычках не обрабатываются!
$Turxen = "Hello! My name is $xuk and i have some {$fruit}'s";
//желательно константы все таки через дефайн определять!
define("PAGE", "new page");
const PAGE_2 = 'new const';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo PAGE;?></title>
    </head>
    <body>
    <h1>
        <?php echo $xuk;  ?>
    </h1>
    <p>
        <?php echo $Turxen; ?>
    </p>
    <b>
        <?php echo PAGE_2 ?>
    </b>
    </body>
</html>