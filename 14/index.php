<?php
error_reporting(0);
echo '<h1><b>Lesson 14 - require и include в php</b></h1><br>';

//include - при подключении несуществующего файла остальной код отработает
//require - вызывает ошибку уровня fatal error - скрипт дальше не отработает!
//для ключевых файлов юзай require, для не ключевых - include.
//require_once и include_once - гарантия единственного подключения файла
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lesson 14</title>
    </head>
    <body>
        <p>lorem ipsum </p>
            <?php include 'inc/inc.php' ?>
        <p>lorem ipsum </p>
    </body>
</html>
